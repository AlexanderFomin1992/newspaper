package com.example.afomin.newspaper;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class NewsListActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    NewsRecyclerAdapter newsRecyclerAdapter;
    RecyclerView.LayoutManager layoutManager;
    NewsListItemDecoration newsListItemDecoration;

    public static final String TAG = "NewsDebug";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager;

        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            // portrait mode
            layoutManager = new LinearLayoutManager(this);
        } else {
            // landscape mode
            layoutManager = new GridLayoutManager(this, 2);
        }
        recyclerView.setLayoutManager(layoutManager);
        newsRecyclerAdapter = new NewsRecyclerAdapter(this, DataUtils.generateNews());
        recyclerView.setAdapter(newsRecyclerAdapter);

        newsListItemDecoration = new NewsListItemDecoration();
        recyclerView.addItemDecoration(newsListItemDecoration);

        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(
                this, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(animation);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case (R.id.about_creator): {
                Intent intent = new Intent(NewsListActivity.this, AboutActivity.class);
                startActivity(intent);
                return true;
            }
            default: {
                return false;
            }
        }
    }
}
