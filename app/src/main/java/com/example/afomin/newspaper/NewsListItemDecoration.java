package com.example.afomin.newspaper;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class NewsListItemDecoration extends RecyclerView.ItemDecoration {

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = parent.getContext().getResources().getInteger(R.integer.news_list_item_offset_bottom);
        outRect.top = parent.getContext().getResources().getInteger(R.integer.news_list_item_offset_top);
        outRect.left = parent.getContext().getResources().getInteger(R.integer.news_list_item_offset_left);
        outRect.right = parent.getContext().getResources().getInteger(R.integer.news_list_item_offset_right);
    }
}
