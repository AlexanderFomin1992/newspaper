package com.example.afomin.newspaper;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class NewsItem implements Parcelable {

    private final String title;
    private final String imageUrl;
    private final Category category;
    private final Date publishDate;
    private final String previewText;
    private final String fullText;

    public NewsItem(
            String title, String imageUrl, Category category, Date publishDate, String previewText, String fullText) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.category = category;
        this.publishDate = publishDate;
        this.previewText = previewText;
        this.fullText = fullText;
    }

    public NewsItem(Parcel in) {
        this.title = in.readString();
        this.imageUrl = in.readString();
        this.category = in.readParcelable(Category.class.getClassLoader());
        this.publishDate = (Date) in.readSerializable();
        this.previewText = in.readString();
        this.fullText = in.readString();
    }

    public static final Creator<NewsItem> CREATOR = new Creator<NewsItem>() {
        @Override
        public NewsItem createFromParcel(Parcel in) {
            return new NewsItem(in);
        }

        @Override
        public NewsItem[] newArray(int size) {
            return new NewsItem[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Category getCategory() {
        return category;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public String getPreviewText() {
        return previewText;
    }

    public String getFullText() {
        return fullText;
    }

    @Override
    public int describeContents() {
        return 0;
    }



    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.title);
        parcel.writeString(this.imageUrl);
        parcel.writeParcelable(this.category, i);
        parcel.writeSerializable(this.publishDate);
        parcel.writeString(this.previewText);
        parcel.writeString(this.fullText);

    }
}
