package com.example.afomin.newspaper;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;

import java.text.SimpleDateFormat;

public class NewsDetailsActivity extends AppCompatActivity {
    public static final String NEWS_ITEM_KEY = "NEWS_ITEM_KEY";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        ImageView imageView = (ImageView)findViewById(R.id.imageViewHeadImage);
        NewsItem newsItem = getIntent().getExtras().getParcelable(NEWS_ITEM_KEY);
        getSupportActionBar().setTitle(newsItem.getCategory().getName());

        ((TextView)findViewById(R.id.textViewTitle)).setText(newsItem.getTitle());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        ((TextView)findViewById(R.id.textViewDate)).setText(
                simpleDateFormat.format(newsItem.getPublishDate()));
        ((TextView)findViewById(R.id.textViewTextDetailed)).setText(newsItem.getFullText());

        RequestOptions imageOption = new RequestOptions()
                .placeholder(R.drawable.avatar_placeholder)
                .fallback(R.drawable.avatar_placeholder);
        RequestManager imageLoader = Glide.with(this).applyDefaultRequestOptions(imageOption);
        imageLoader.load(newsItem.getImageUrl()).into(imageView);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case (R.id.about_creator): {
                Intent intent = new Intent(NewsDetailsActivity.this, AboutActivity.class);
                startActivity(intent);
                return true;
            }
            default: {
                return false;
            }
        }
    }
}
