package com.example.afomin.newspaper;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;

import java.text.SimpleDateFormat;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NewsRecyclerAdapter extends RecyclerView.Adapter<NewsRecyclerAdapter.ViewHolder> {

    private final List<NewsItem> newsItems;
    private final LayoutInflater layoutInflater;
    private final RequestManager imageLoader;

    public NewsRecyclerAdapter(
            Context context,
            List<NewsItem> newsItems) {
        this.newsItems = newsItems;
        this.layoutInflater = LayoutInflater.from(context);
        RequestOptions imageOption = new RequestOptions()
                .placeholder(R.drawable.avatar_placeholder)
                .fallback(R.drawable.avatar_placeholder);
        this.imageLoader = Glide.with(context).applyDefaultRequestOptions(imageOption);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.news_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(newsItems.get(position));
    }

    @Override
    public int getItemCount() {
        return newsItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView headImageView;
        private final TextView textViewTitle;
        private final TextView textViewPreview;
        private final TextView textViewCategory;
        private final TextView textViewDate;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            headImageView = itemView.findViewById(R.id.imageViewHeadImage);
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewPreview = itemView.findViewById(R.id.textViewPreviewText);
            textViewDate = itemView.findViewById(R.id.textViewDate);
            textViewCategory = itemView.findViewById(R.id.textViewCategory);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   int position = getAdapterPosition();
                   if (position != RecyclerView.NO_POSITION) {
                       NewsItem newsItem = newsItems.get(position);
                       Intent intent = new Intent(view.getContext(), NewsDetailsActivity.class);
                       intent.putExtra(NewsDetailsActivity.NEWS_ITEM_KEY, newsItem);
                       view.getContext().startActivity(intent);
                       Log.d(NewsListActivity.TAG, String.format("clicked news with title: %s", newsItem.getTitle()));
                   }
                }
            });

        }

        void bind(NewsItem newsItem) {
            imageLoader.load(newsItem.getImageUrl()).into(headImageView);
            textViewTitle.setText(newsItem.getTitle());
            textViewPreview.setText(newsItem.getPreviewText());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
            textViewDate.setText(simpleDateFormat.format(newsItem.getPublishDate()));
            textViewCategory.setText(newsItem.getCategory().getName());
        }
    }
}
